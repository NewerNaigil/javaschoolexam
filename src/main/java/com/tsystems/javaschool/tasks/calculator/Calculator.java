package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.EmptyStackException;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String answer = rpnToAnswer(expressionToRPN(statement));
        // TODO: Implement the logic here
        return answer;
    }

    public String expressionToRPN (String statement){
        String outArray = "";
        Stack<Character> stack = new Stack<>();
        int priority;

        if (statement == null || statement == ""){
            return null;
        }

        for (int i = 0; i < statement.length(); i++){
            priority = getPriority(statement.charAt(i));
            if (priority == 0){
                outArray += statement.charAt(i);
            }
            if (priority == 1){
                stack.push(statement.charAt(i));
            }

            if (priority > 1){
                outArray +=' ';
                while (!stack.empty()){
                    if (getPriority(stack.peek()) >= priority){
                        outArray += stack.pop();
                    }
                    else {
                        break;
                    }
                }
                stack.push(statement.charAt(i));
            }

            try {
                if (priority == -1){
                    outArray +=' ';
                    while (getPriority(stack.peek()) != 1){
                        outArray += stack.pop();
                    }
                    stack.pop();
                }
            }
            catch (EmptyStackException ex){
                return null;
            }
        }

        while (!stack.empty()){
            outArray += stack.pop();
        }

        for (int i = 0; i < outArray.length(); i++) {
            if (outArray.charAt(i) == '(' || outArray.charAt(i) == ')') {
                return null;
            }
        }

        return outArray;
    }

    public String rpnToAnswer (String rpn) {
        String var = "";
        Stack<Double> stack = new Stack<>();

        if (rpn == null){
            return null;
        }

        for (int i = 0; i < rpn.length(); i++){
            if (rpn.charAt(i) == ' '){
                continue;
            }

            try {
                if (getPriority(rpn.charAt(i)) == 0){
                    while (rpn.charAt(i) != ' ' && getPriority(rpn.charAt(i)) == 0 ){
                        var += rpn.charAt(i);
                        i++;
                        if (i == rpn.length()){
                            break;
                        }
                    }
                    stack.push(Double.parseDouble(var));
                    var = "";
                }
            }
            catch (NumberFormatException ex){
                return null;
            }

            if (getPriority(rpn.charAt(i)) > 1){
                try {
                    double a = stack.pop();
                    double b = stack.pop();

                    if (rpn.charAt(i) == '+'){
                        stack.push(b + a);
                    }
                    if (rpn.charAt(i) == '-'){
                        stack.push(b - a);
                    }
                    if (rpn.charAt(i) == '*'){
                        stack.push(b * a);
                    }
                    if (rpn.charAt(i) == '/'){
                        if (a != 0) {
                            stack.push(b / a);
                        }
                        else {
                            return null;
                        }
                    }
                }
                catch (EmptyStackException ex){
                    return null;
                }
            }
        }
        return answerFormat(stack.pop());
    }

    private static int getPriority (char val){
        if (val == '*' || val == '/') {
            return 3;
        }
        if (val == '+' || val == '-' ) {
            return 2;
        }
        if (val == '(') {
            return 1;
        }
        if (val == ')') {
            return -1;
        }
        else {
            return 0;
        }
    }

    private String answerFormat (double preAnswer){

        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.ENGLISH);
        DecimalFormat df = new DecimalFormat("#.####", dfs);

        int number;
        double fraction;

        number = (int) preAnswer;
        fraction = preAnswer - number;

        if (fraction == 0){
            return Integer.toString(number);
        }
        else {
            return df.format(preAnswer);
        }
    }
}
