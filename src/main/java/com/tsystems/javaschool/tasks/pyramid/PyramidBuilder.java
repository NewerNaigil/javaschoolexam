package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        boolean flag;
        int[][] matrix;
        List<Integer> sorted;

        int size = inputNumbers.size();
        int counter = 0;
        int rows = 1;
        int cols = 1;

        while (counter < size){
            counter += rows;
            rows++;
            cols += 2;
        }

        rows -= 1;
        cols -= 2;

        if(size == counter) {
            flag = true;
        }
        else {
            flag = false;
        }

        if (!flag){
            throw new CannotBuildPyramidException();
        }

        try {
            sorted = inputNumbers.stream().sorted().collect(Collectors.toList());
        }
        catch (IllegalArgumentException | NullPointerException ex){
            throw new CannotBuildPyramidException();
        }

        matrix = new int [rows][cols];
        for (int[] row : matrix){
            Arrays.fill(row, 0);
        }

        int center = (cols / 2);
        counter = 1;
        int arrays = 0;

        for (int i = 0, offset = 0; i < rows; i++, offset++, counter++){
            int start = center - offset;
            for (int j = 0; j < counter * 2; j += 2, arrays++){
                matrix[i][start + j] = sorted.get(arrays);
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
        return matrix;
    }
}
